let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return (collection);
}

//Adds an element/elements to the end of the queue
function enqueue(item){

    let length = 0;

      while(collection[length]!==undefined){
        length++;

      }

    collection[length] = item;

    return collection;
}

//Removes an element in front of the queue
function dequeue(){

 let newArr = [];

 let length1 = 0;

      while(collection[length1]!==undefined){
        length1++;
      }

 let length2 = 0;

      while(newArr[length2]!==undefined){
        length2++;
      }


   for (let i = 0; i < length1; i++) {
        if(collection[i] !== collection[0]) {
            newArr[length2] = collection[i];
            collection = newArr;
        }
    }
    return newArr;
}


//Shows the element at the front
function front(){
    return collection[0];
}

//Shows the total number of elements
function size(){
    
    let length = 0;

      while(collection[length]!==undefined){
        length++;

      }

    return length;
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){

    let length = 0;

      while(collection[length]!==undefined){
        length++;

      }

    if (length === []){
        return true;
    } else {
        return false;
    }
}



// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};